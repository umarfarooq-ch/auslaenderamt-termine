import re
import time
from datetime import datetime
from pathlib import Path

import requests
from bs4 import BeautifulSoup
from django.conf import settings
from django.core.mail import send_mail


def send_notification_email(appointments, to, *args, **kwargs):
    """
    Args:
        :param str to: email address to notify for available appointments'
        :param appointments: dictionary {str(day): str(list of strings containing time slots) }
        args: None
        kwargs: None
    Returns:
        None
    """
    subject = f'Ausländeramt termine available'
    email_from = settings.EMAIL_HOST_USER
    email_to = to
    message = "Following 30min appointments for Ausländeramt are available:<br><br>"
    for day_and_date in appointments:
        message += f'* {day_and_date}: {appointments[day_and_date]} <br>'
    message += '<br><br>Link: https://termine.staedteregion-aachen.de/auslaenderamt/?rs'
    send_mail(
        subject=subject,
        message=message,
        from_email=email_from,
        recipient_list=[email_to],
        html_message=message,
    )


def search_termine(*args, **kwargs):
    counter = 1
    while (True):
        d = requests.get('https://termine.staedteregion-aachen.de/auslaenderamt/select2?md=1')
        cookie = {'TVWebSession': d.cookies._cookies['termine.staedteregion-aachen.de']['/']['TVWebSession'].value}

        # step number 3
        s3 = requests.get(
            'https://termine.staedteregion-aachen.de/auslaenderamt/suggest?mdt=67&select_cnc=1&cnc-204=0&cnc-205=0&cnc-198=0&cnc-201=0&cnc-202=0&cnc-216=0&cnc-189=0&cnc-203=0&cnc-196=0&cnc-200=0&cnc-199=0&cnc-188=0&cnc-186=0&cnc-193=0&cnc-183=0&cnc-184=0&cnc-185=1&cnc-187=0&cnc-190=0&cnc-195=0&cnc-191=0&cnc-194=0&cnc-197=0&cnc-192=0',
            cookies=cookie,
        )

        # update date and time
        today = datetime.now().strftime("%B %d, %Y : %H:%M:%S ")

        # send email if appointment available
        if 'Kein freier Termin verfügbar' not in s3.text:
            path = 'HTMLs'
            path_obj = Path(path)
            path_obj.mkdir(exist_ok=True)
            (path_obj / f'HTML-{counter}.html').write_text(s3.text)
            print(f"Appointment available ------------------------------------------------------------------------------  {today} --- counter: {counter}")
            counter += 1

            dic = {}
            soup = BeautifulSoup(s3.content, 'html.parser')
            # extract day and date divs
            day_divs = soup.find_all('h3', title=re.compile(r'\d\d\.\d\d.\d\d\d\d'))
            # extract slots divs
            slot_divs = day_divs[0].parent.find_all(class_='sugg_table')

            for day_div, slot_div in zip(day_divs, slot_divs):

                # extract time slots in text
                time_slots_of_a_day = [x.text for x in slot_div.find_all('button', class_='suggest_btn btn btn-primary', disabled=None)]
                # extract day and date
                day_and_date = day_div['title']

                # add into dictionary
                dic[f'{day_and_date}'] = time_slots_of_a_day

            # send email
            for email in settings.EMAILS_TO_NOTIFY:
                send_notification_email(dic, email)

        else:
            print(f"No appointments available --------------------------------------------------------------------------  {today}")

        time.sleep(settings.SCAN_INTERVAL)
