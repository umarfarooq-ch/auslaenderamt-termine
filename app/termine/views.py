# Create your views here.
from rest_framework.response import Response
from rest_framework.views import APIView

from .utils import search_termine


class AppointmentView(APIView):

    def get(self, request, *args, **kwargs):
        import threading
        t = threading.Thread(target=search_termine)
        t.name = 'EmailService'
        t.deamon = True
        t.start()
        return Response({'data': 'Thread started'})
