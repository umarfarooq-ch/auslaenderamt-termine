from django.urls import path

from termine import views

urlpatterns = [
    path('', views.AppointmentView.as_view(), name='startView'),
    path('api/start_scaning/', views.AppointmentView.as_view(), name='startView'),

]
