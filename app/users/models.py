from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    """
    Custom user model as recommended by Django documentation
    https://docs.djangoproject.com/en/4.1/topics/auth/customizing/#using-a-custom-user-model-when-starting-a-project
    """
    pass
