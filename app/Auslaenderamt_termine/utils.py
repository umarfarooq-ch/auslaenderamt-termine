from django.core.mail import send_mail


def send_notification_email(*args, **kwargs):
    """
    Args:
        args: None
        kwargs: None
    Returns:
        None
    """
    subject = f'Termine available'
    email_from = "umarfarooq2000@gamil.com"
    email_to = "umarfarooq2000@gmail.com"

    message = "Appointment of Ausländeramt is available\n\n. Link: https://termine.staedteregion-aachen.de/auslaenderamt/?rs"

    send_mail(
        subject=subject,
        message=message,
        from_email=email_from,
        recipient_list=[email_to],
        html_message=message,
    )