# Foreign office appointment notifier

## Objective:
Due to very high demand and low staff, it is very difficult to get an appointment at a foreign office for a residence permit issue/renewal. Very few appointments lists on the Foreign Office website, which appears once or twice a week and vanishes instantly. I created this app, which basically notifies by email when an appointment is available.






# Installations:
Steps to run django application server:
============
## 1. Dependencies:
To be able to run **server** you have to meet following dependencies:

- python3.10, python3.10-dev, pip3, and pipenv
- curl

## 2. Install App Requirements:
- Switch to project root directory.
- Run `$ pip install -r requirements.txt`

## 3. Configurations:
- Create the `.env` file  in `/$PROJECT_ROOT/Auslaenderamt_termine/` with following entries.
```
EMAIL_HOST=smtp.gmail.com
EMAIL_USER=<email_address@server.com>
EMAIL_PASSWORD=<email_password> 
SCAN_INTERVAL=5
```
Note: SCAN_INTERVAL is in minutes, don't decrease the interval, it will increase load on the foreign office server. 5 minutes is ideal. You can get an appointment easily. 

## 4. Apply migrations:
- Switch to project root directory.
- Run `$ python manage.py migrate`

## 5. Start Application Server:
- Switch to project root directory.

- **for development server**
  
  Run `$ python manage.py runserver`


server is now up on `host:8000`

## 6. Hit the rest API to start scanning
Run `$ curl --location --request GET 'localhost:8000/start/`

## Future work
The project has high potential to add new features and remove redundant notifications and log files.