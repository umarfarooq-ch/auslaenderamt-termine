# Auslaenderamt termine(Foreign office appointment) notifier

## Objective:
Due to very high demand and low staff, it is very difficult to get an appointment at a foreign office for a residence permit issue/renewal. Very few appointments lists on the Foreign Office website, which appears once or twice a week and vanishes instantly. I created this app, which basically notifies by email when an appointment is available.



# Installations
Steps to run django application server:
============
## 1. Dependencies:
To be able to run **Auslaenderamt termine server** you have to meet following dependencies:

- docker docker-compose
- libkrb5-dev

## 2. Configurations:
- Create `.env` file in `/$PROJECT_ROOT/app/Auslaenderamt_termine/` directory

Sample .env
```
# Django app configs
SECRET_KEY="SECRET-KEY"
DEBUG=0

# Email configs
EMAIL_HOST=smtp.gmail.com
EMAIL_USER=sending_email_address@gmail.com
EMAIL_PASSWORD=email_password
SCAN_INTERVAL=60
EMAILS_TO_NOTIFY=email_1_to_notify@example.com,email_2_to_notify@example.com

# MariaDB
MYSQL_ROOT_PASSWORD=mariadb
MYSQL_HOST=db
MYSQL_DATABASE=auslaenderamt_termine
MYSQL_USER=root
MYSQL_PASSWORD=mariadb
```

Notes on .env variables

- `SCAN_INTERVALS` is in minutes, for example 60 means, after 60 minutes it will scan for new appointments.
- `EMAILS_TO_NOTIFY` is the comma seperated emails to notify the users about available appointments. 


## 3. Install App Requirements:
- Switch to project root directory.
- Run `$ docker-compose up --build`



Auslaenderamt termine's docker application is now up on `https://host`
