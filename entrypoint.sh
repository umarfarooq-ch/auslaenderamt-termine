#!/bin/bash

# This script is executed when the Django Docker container is started.

echo "Migrate ... "
python3 manage.py migrate --noinput
echo "done"

echo "Collectstatics ... "
python3 manage.py collectstatic --noinput
echo "done"

echo "App initialized ..."


echo "Starting project..."



# The uvicorn ASGI can automatically reload the server in case of a code change.
# This feature can be disabled by removing the "--reload" flag or by using the daphne asgi.
uvicorn --host 0.0.0.0 --port 8000 Auslaenderamt_termine.asgi:application